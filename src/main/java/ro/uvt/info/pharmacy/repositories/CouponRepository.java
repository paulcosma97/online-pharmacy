package ro.uvt.info.pharmacy.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.uvt.info.pharmacy.entities.Coupon;

import java.util.Optional;

public interface CouponRepository extends JpaRepository<Coupon, Integer> {
    Optional<Coupon> findByCode(String code);
}
