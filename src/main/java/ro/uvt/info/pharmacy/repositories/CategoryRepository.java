package ro.uvt.info.pharmacy.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.uvt.info.pharmacy.entities.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
