package ro.uvt.info.pharmacy.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ro.uvt.info.pharmacy.entities.Report;

import java.time.LocalDate;

public interface ReportRepository extends JpaRepository<Report, Integer> {
    int countAllByCreatedOnBeforeAndCreatedOnAfter(LocalDate before, LocalDate after);

    @Query("select max(report.id) from Report report")
    Integer getMaxId();

    default int countAllByCurrentDay() {
        return countAllByCreatedOnBeforeAndCreatedOnAfter(LocalDate.now().plusDays(1), LocalDate.now().minusDays(1));
    }

    default int countAllByCurrentWeek() {
        return countAllByCreatedOnBeforeAndCreatedOnAfter(LocalDate.now().plusDays(7), LocalDate.now().minusDays(7));
    }

    default int countAllByCurrentMonth() {
        return countAllByCreatedOnBeforeAndCreatedOnAfter(LocalDate.now().plusMonths(1), LocalDate.now().minusMonths(1));
    }

    default int countAllByCurrentYear() {
        return countAllByCreatedOnBeforeAndCreatedOnAfter(LocalDate.now().plusYears(1), LocalDate.now().minusYears(1));
    }
}
