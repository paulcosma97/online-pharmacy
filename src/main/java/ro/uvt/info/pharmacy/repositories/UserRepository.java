package ro.uvt.info.pharmacy.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.uvt.info.pharmacy.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsernameAndPassword(String username, String password);
}
