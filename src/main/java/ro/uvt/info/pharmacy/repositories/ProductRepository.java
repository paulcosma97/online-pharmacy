package ro.uvt.info.pharmacy.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.uvt.info.pharmacy.entities.Category;
import ro.uvt.info.pharmacy.entities.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    List<Product> findAllByNameContaining(String partialName);

    List<Product> findAllByNameContainingAndCategory(String partialName, Category category);
}
