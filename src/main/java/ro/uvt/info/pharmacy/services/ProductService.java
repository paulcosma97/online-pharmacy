package ro.uvt.info.pharmacy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ro.uvt.info.pharmacy.entities.Category;
import ro.uvt.info.pharmacy.entities.Product;
import ro.uvt.info.pharmacy.repositories.ProductRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProductService {
    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void createProduct(Product product) {
        productRepository.save(product);
    }

    public void updateProduct(Product product, int id) {
        product.setId(id);

        productRepository.save(product);
    }

    public Product findProductById(int id) {
        if(!productRepository.existsById(id)) {
            throw new EntityNotFoundException();
        }

        return productRepository.getOne(id);
    }

    public void deleteProduct(int id) {
        if(!productRepository.existsById(id)) {
            throw new EntityNotFoundException();
        }

        productRepository.deleteById(id);
    }

    public List<Product> productPage(int page, int size) {
        return productRepository.findAll(PageRequest.of(page, size)).getContent();
    }

    public List<Product> searchProductsByName(String name) {
        return productRepository.findAllByNameContaining(name);
    }

    public List<Product> searchProductsByNameAndCategory(String name, Category category) {
        return productRepository.findAllByNameContainingAndCategory(name, category);
    }
}
