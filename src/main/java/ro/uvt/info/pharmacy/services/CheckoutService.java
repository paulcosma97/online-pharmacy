package ro.uvt.info.pharmacy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.uvt.info.pharmacy.entities.Product;
import ro.uvt.info.pharmacy.repositories.ProductRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Map;
import java.util.Optional;

@Service
public class CheckoutService {
    private ProductRepository productRepository;

    @Autowired
    public CheckoutService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void addToCheckout(int productId, Map<Product, Integer> products) {
        Optional<Product> optionalProduct = products.keySet().parallelStream().filter(p -> p.getId() == productId).findFirst();
        Product product = optionalProduct.orElseGet(() -> productRepository.findById(productId).orElseThrow(EntityNotFoundException::new));

        if(!products.containsKey(product)) {
            products.put(product, 0);
        }
        products.replace(product, products.get(product) + 1);
    }

    public void removeFromCheckout(int productId, Map<Product, Integer> products) {
        Product product = products.keySet().parallelStream().filter(p -> p.getId() == productId).findAny().get();

        products.replace(product, products.get(product) - 1);

        if(products.get(product) == 0) {
            products.remove(product);
        }
    }
}
