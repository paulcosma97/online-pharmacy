package ro.uvt.info.pharmacy.utils;

import ro.uvt.info.pharmacy.entities.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionHelper {
    private HttpSession session;

    private SessionHelper(HttpServletRequest request) {
        this.session = request.getSession();
    }

    public static SessionHelper using(HttpServletRequest request) {
        return new SessionHelper(request);
    }

    public User getUser() {
        return ((User) session.getAttribute("loggedUser"));
    }

    public SessionHelper setUser(User user) {
        session.setAttribute("loggedUser", user);
        return this;
    }
}
