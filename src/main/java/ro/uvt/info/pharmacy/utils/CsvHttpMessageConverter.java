package ro.uvt.info.pharmacy.utils;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractGenericHttpMessageConverter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
public class CsvHttpMessageConverter extends AbstractGenericHttpMessageConverter<List> {

    public CsvHttpMessageConverter() {
        super(new MediaType("text", "csv"));
    }

    /**
     * Converts a CSV body to a list of objects
     * @param clazz class of object to convert
     * @param inputStream stream to read the CSV from
     * @param <T> type of object
     * @return list of converted objects
     */
    @SuppressWarnings("unchecked")
    @SneakyThrows
    public static <T> List<T> fromCsv(Class<T> clazz, InputStream inputStream) throws IOException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();

        MappingIterator mappingIterator = csvMapper.readerFor(clazz).with(schema).readValues(inputStream);
        return mappingIterator.readAll();
    }

    /**
     * Converts any list of objects to CSV
     * @param clazz class of the object to convert
     * @param objList list of objects to convert
     * @param outputStream stream to write the CSV to
     * @param <T> type of object
     */
    public static <T> void toCsv(Class<T> clazz, List<T> objList, OutputStream outputStream) throws IOException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema schema = csvMapper
            .schemaFor(clazz)
            .withHeader()
            .withColumnReordering(false);
        ObjectWriter objectWriter = csvMapper.writerFor(clazz).with(schema);
        objectWriter.writeValuesAsArray(outputStream).writeAll(objList);
        outputStream.close();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void writeInternal(List objList, Type type, HttpOutputMessage outputMessage) throws IOException {
        Type actualType = ((ParameterizedType) type).getActualTypeArguments()[0];

        try {
            toCsv(Class.forName(actualType.getTypeName()), objList, outputMessage.getBody());
        } catch (ClassNotFoundException e) {
            log.error("Class not found.", e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected List readInternal(Class clazz, HttpInputMessage inputMessage) throws IOException {
        return fromCsv(clazz, inputMessage.getBody());
    }

    @Override
    public List read(Type type, Class contextClass, HttpInputMessage inputMessage) throws IOException {
        Type actualType = ((ParameterizedType) type).getActualTypeArguments()[0];
        try {
            return fromCsv(Class.forName(actualType.getTypeName()), inputMessage.getBody());
        } catch (ClassNotFoundException e) {
            log.error("Class not found.", e);
        }
        throw new RuntimeException();
    }

    @Override
    public boolean canRead(Type type, Class<?> contextClass, MediaType mediaType) {
        boolean isList = false;
        if (type instanceof ParameterizedType) {
            isList = ((ParameterizedType) type).getRawType().getTypeName().equalsIgnoreCase(List.class.getName());
        }

        return isList && super.canRead(type, contextClass, mediaType);
    }

    @Override
    public boolean canWrite(Type type, Class<?> clazz, MediaType mediaType) {
        boolean isList = false;
        if (type instanceof ParameterizedType) {
            isList = ((ParameterizedType) type).getRawType().getTypeName().equalsIgnoreCase(List.class.getName());
        }

        return isList && super.canWrite(type, clazz, mediaType);
    }
}
