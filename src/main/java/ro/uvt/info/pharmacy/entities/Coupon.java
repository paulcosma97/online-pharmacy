package ro.uvt.info.pharmacy.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Coupon {
    @Id
    @GeneratedValue
    private int id;
    @Column(unique = true, length = 5)
    private String code;
    private int discount;
}
