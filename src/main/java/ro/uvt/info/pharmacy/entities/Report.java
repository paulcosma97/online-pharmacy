package ro.uvt.info.pharmacy.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Data
@Entity
@AllArgsConstructor
@Accessors(chain = true)
public class Report {
    @Id
    @GeneratedValue
    private int id;
    @ManyToOne
    private Product product;
    private String code;
    private LocalDate createdOn;
}
