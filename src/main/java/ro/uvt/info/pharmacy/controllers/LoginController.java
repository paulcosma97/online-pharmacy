package ro.uvt.info.pharmacy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.uvt.info.pharmacy.entities.User;
import ro.uvt.info.pharmacy.services.UserService;
import ro.uvt.info.pharmacy.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/login")
public class LoginController {
    private UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getIndex(@RequestParam(name = "logout", required = false) String logout, HttpServletRequest request) {
        if(logout != null) {
            SessionHelper.using(request).setUser(null);
            return "redirect:/";
        }


        User user = SessionHelper.using(request).getUser();

        if(user != null) {
            return "redirect:/";
        }

        return "login";
    }

    @PostMapping
    public String login(@ModelAttribute User user, HttpServletRequest request) {
        SessionHelper.using(request).setUser( userService.findUser(user) );
        return "redirect:/";
    }
}
