package ro.uvt.info.pharmacy.controllers;

import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ro.uvt.info.pharmacy.entities.Coupon;
import ro.uvt.info.pharmacy.repositories.CategoryRepository;
import ro.uvt.info.pharmacy.repositories.ProductRepository;
import ro.uvt.info.pharmacy.repositories.ReportRepository;
import ro.uvt.info.pharmacy.utils.CsvHttpMessageConverter;
import ro.uvt.info.pharmacy.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private ReportRepository reportRepository;
    private ProductRepository productRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public AdminController(ReportRepository reportRepository, ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.reportRepository = reportRepository;
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    @GetMapping
    public String getIndex(Model model, HttpServletRequest request) {
        if(SessionHelper.using(request).getUser() == null) {
            return "redirect:/login";
        }

        model.addAttribute("report", new ReportData(reportRepository.countAllByCurrentDay(), reportRepository.countAllByCurrentWeek(), reportRepository.countAllByCurrentMonth(), reportRepository.countAllByCurrentYear()));
        model.addAttribute("coupon", new Coupon());
        return "admin_panel";
    }

    @PostMapping
    @SneakyThrows
    public String importCsv(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        if(SessionHelper.using(request).getUser() == null) {
            return "redirect:/login";
        }

        val productDtos = CsvHttpMessageConverter.fromCsv(ProductController.ProductDto.class, file.getInputStream());
        productRepository.flush();
        productRepository.saveAll(productDtos.parallelStream().map(productDto -> productDto.toInbound(categoryRepository.getOne(productDto.getCategory()))).collect(Collectors.toList()));
        return "redirect:/admin";
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class ReportData {
        private int day;
        private int week;
        private int month;
        private int year;
    }
}
