package ro.uvt.info.pharmacy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.uvt.info.pharmacy.entities.Coupon;
import ro.uvt.info.pharmacy.repositories.CouponRepository;

@Controller
@RequestMapping("/coupon")
public class CouponController {
    private CouponRepository couponRepository;

    @Autowired
    public CouponController(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    @PostMapping
    public String create(@ModelAttribute Coupon coupon) {
        couponRepository.save(coupon.setId(0));
        return "redirect:/admin";
    }
}
