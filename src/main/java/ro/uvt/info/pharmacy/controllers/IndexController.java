package ro.uvt.info.pharmacy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.uvt.info.pharmacy.entities.Product;
import ro.uvt.info.pharmacy.services.CategoryService;
import ro.uvt.info.pharmacy.services.ProductService;

import java.util.List;

@Controller
@RequestMapping("/")
public class IndexController {
    private ProductService productService;
    private CategoryService categoryService;

    @Autowired
    public IndexController(ProductService productService, CategoryService categoryService) {
        this.productService = productService;
        this.categoryService = categoryService;
    }

    @GetMapping
    public String getHome(Model model) {
        List<Product> mostViewedProducts = productService.productPage(0, 8);
        int mostViewedProductsRows = mostViewedProducts.size() / 4;

        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("mostViewedProducts", mostViewedProducts);
        model.addAttribute("mostViewedProductsRows", mostViewedProductsRows);
        return "index";
    }
}
