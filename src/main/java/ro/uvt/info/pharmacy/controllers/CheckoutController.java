package ro.uvt.info.pharmacy.controllers;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ro.uvt.info.pharmacy.entities.Product;
import ro.uvt.info.pharmacy.entities.Report;
import ro.uvt.info.pharmacy.repositories.CouponRepository;
import ro.uvt.info.pharmacy.repositories.ReportRepository;
import ro.uvt.info.pharmacy.services.CheckoutService;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Controller
@RequestMapping("/checkout")
@Scope("session")
public class CheckoutController {
    private ReportRepository reportRepository;
    private CouponRepository couponRepository;

    private Map<Product, Integer> products = new HashMap<>();
    private CheckoutService checkoutService;

    @Autowired
    public CheckoutController(CheckoutService checkoutService, ReportRepository reportRepository, CouponRepository couponRepository) {
        this.checkoutService = checkoutService;
        this.reportRepository = reportRepository;
        this.couponRepository = couponRepository;
    }

    @PostMapping(path = "/add/{productId}")
    public String add(@PathVariable("productId") int productId) {
        checkoutService.addToCheckout(productId, products);
        return "redirect:/checkout";
    }

    @PostMapping(path = "/remove/{productId}")
    public String remove(@PathVariable("productId") int productId) {
        checkoutService.removeFromCheckout(productId, products);
        return "redirect:/checkout";
    }

    @GetMapping
    public String get(Model model) {
        model.addAttribute("totalPrice", String.format("%.2f", getTotalPrice()));
        model.addAttribute("products", products);
        return "checkout";
    }

    @GetMapping("/finalize")
    public String proceed(Model model, @RequestParam(required = false) String couponCode) {
        val discount = couponCode == null || couponCode.equals("") ? 1.0 : ((100 - couponRepository.findByCode(couponCode).orElseThrow(EntityNotFoundException::new).getDiscount()) / 100.0);
        model.addAttribute("totalPrice", String.format("%.2f", getTotalPrice() * discount));

        val max = reportRepository.getMaxId();
        val code = (max == null ? 0 : max) + 1 + "_" + (new Random().nextInt(1000) + 1000);

        for(Map.Entry<Product, Integer> entry : products.entrySet()) {
            for(int i = 0; i < entry.getValue(); i++) {
                val report = new Report(0, entry.getKey(), code, LocalDate.now());
                reportRepository.save(report);
            }
        }

        model.addAttribute("code", code);

        products.clear();
        return "checkout_code";
    }

    private double getTotalPrice() {
        double totalPrice = 0;
        for(Map.Entry<Product, Integer> entry : products.entrySet()) {
            totalPrice += entry.getKey().getPrice() * entry.getValue();
        }

        return totalPrice;
    }
}
