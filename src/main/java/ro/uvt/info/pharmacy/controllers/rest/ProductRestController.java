package ro.uvt.info.pharmacy.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ro.uvt.info.pharmacy.entities.Product;
import ro.uvt.info.pharmacy.services.ProductService;

import java.util.List;

@RestController
@RequestMapping("/rest/products")
public class ProductRestController {
    private ProductService productService;

    @Autowired
    public ProductRestController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(path = { "/{page}", "" })
    public List<Product> getProducts(@PathVariable(required = false) Integer page) {
        return productService.productPage(page == null ? 0 : page, 10);
    }

    @GetMapping(path = "/{productId}")
    public Product getProduct(@PathVariable int productId) {
        return productService.findProductById(productId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createProduct(@RequestBody Product Product) {
        productService.createProduct(Product);
    }

    @PutMapping(path = "/{productId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateProduct(@PathVariable int productId, @RequestBody Product Product) {
        productService.updateProduct(Product, productId);
    }

    @DeleteMapping(path = "/{productId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable int productId) {
        productService.deleteProduct(productId);
    }
}
