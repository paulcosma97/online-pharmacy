package ro.uvt.info.pharmacy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.uvt.info.pharmacy.entities.Category;
import ro.uvt.info.pharmacy.entities.Product;
import ro.uvt.info.pharmacy.services.ProductService;

import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {
    private ProductService productService;

    @Autowired
    public SearchController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public String search(@RequestParam("name") String name, @RequestParam(name = "category", required = false) Category category, Model model) {
        List<Product> searchResults = category == null ? productService.searchProductsByName(name) : productService.searchProductsByNameAndCategory(name, category);

        model.addAttribute("searchedName", name);
        model.addAttribute("searchedCategory", category);
        model.addAttribute("products", searchResults);
        return "search_results";
    }
}
