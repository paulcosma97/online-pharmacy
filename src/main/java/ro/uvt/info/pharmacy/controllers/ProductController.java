package ro.uvt.info.pharmacy.controllers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ro.uvt.info.pharmacy.entities.Category;
import ro.uvt.info.pharmacy.entities.Product;
import ro.uvt.info.pharmacy.repositories.CategoryRepository;
import ro.uvt.info.pharmacy.repositories.ProductRepository;
import ro.uvt.info.pharmacy.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/product")
public class ProductController {
    private CategoryRepository categoryRepository;
    private ProductRepository productRepository;

    @Autowired
    public ProductController(CategoryRepository categoryRepository, ProductRepository  productRepository) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
    }

    @GetMapping(path = "/{product}")
    public String getProduct(@PathVariable Product product, Model model) {
        model.addAttribute("product", product);
        return "product";
    }

    @GetMapping(path = "/add")
    public String getAddProduct(Model model, HttpServletRequest request) {
        if(SessionHelper.using(request).getUser() == null) {
            return "redirect:/login";
        }

        model.addAttribute("product", new ProductDto());
        model.addAttribute("categories", categoryRepository.findAll());
        return "product_admin";
    }

    @PostMapping(path = "/add")
    public String postAddProduct(@ModelAttribute("product") ProductDto product, HttpServletRequest request) {
        if(SessionHelper.using(request).getUser() == null) {
            return "redirect:/login";
        }

        val out = product.toInbound(categoryRepository.getOne(product.getCategory()));
        productRepository.save(out);
        return "redirect:/product/" + out.getId();
    }

    @PostMapping(path = "/refill/{product}")
    public String postRefillProduct(@PathVariable Product product, HttpServletRequest request) {
        if(SessionHelper.using(request).getUser() == null) {
            return "redirect:/login";
        }

        productRepository.save( product.setStock( 100 ) );
        return "redirect:/product/" + product.getId();
    }

    @PostMapping(path = "/delete/{product}")
    public String postDeleteProduct(@PathVariable Product product, HttpServletRequest request) {
        if(SessionHelper.using(request).getUser() == null) {
            return "redirect:/login";
        }

        productRepository.delete(product);
        return "redirect:/";
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ProductDto {
        private String name;
        private String description;
        private String producer;
        private double price;
        private int stock;
        private int category;

        public Product toInbound(Category category) {
            return new Product(0, name, description, producer, price, "/images/product2.png", stock, category);
        }
    }
}
